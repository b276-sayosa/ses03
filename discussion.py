# Python has several structures to store collections or multuple items in a single variable
# List[], dictionary, tuples(), set{}
# Tuples - ("Math", "Science", "English")
# Set - {}"Math", "Science", "English"}

#[SECTION] Lists
# Lists are similar to arrays of JS in a sense that they can contain a collection of data
# To create a listm the square bracket ([]) is used

names = ["John", "George", "Ringo"] #string list
programs = ["developer career", "pi-shape", "short courses"]

truth_variables = [True, False, True, True, False] #boolean list

# A list can contain elements with different data types:
sample_list = ["Apple", 3, False, "Potato", 4, False]

# Note: Problems may arise if you try to use lists with multiple type for something that expects them to be all the same type

# Getting the list size
# The number of elements in a list can be counted using the len() method

print(len(programs))

var_len = len(sample_list)
print(var_len)

# Accessing values/elements in a list
# List can be accessed by providing the index number of the element
# it can be accessed using the negative index

print(names[0])
# Access the last item in the list
print(names[-1])

print(programs[1])
# Access the whole list
print(programs)

# Access a range of values
print(programs[0:2])


# Updating lists 
print(f"Current value : {programs[2]}")
# Update the value
programs[2] = 'ShortCourses'
print(f"Newvalue: {programs[2]}")

# [SECTION] List Manipulation
# List has methods that can be used to manipulate the elements within
# Adding List Items - the append() method allows to insert items to a list
programs.append("global")
print(programs)

# Deleting List items  - the "del" keyword can be used to delete elements in the lists

durations = [260, 180, 20] #number list
durations.append(360)
print(durations)

del durations[-1]
print(durations)

del durations[0]
print(durations)

#insert method
durations.insert(0, 100)
print(durations)

durations.insert(1, 99)
print(durations)

# Membership checking - the "in" keyword checks if the element is in the list
# returns True or False value
print(20 in durations) #TRUE value
print(500 in durations) #False value

# Sorting Lists  - The sort() method sorts the list alpha numerically, ascending, by default.

durations.sort()
print(durations)

# emptying the list - the clear () method is used to empty the contents of the list

test_list = [1, 3, 5, 7, 9]
print(test_list)
test_list.clear()
print(test_list)


#[SECTION] Dictionaries are used to store data values in key : value pairs. This is similar to objects in JS/javaScript
# A dictionary is a collection which is ordered, changeable and does not allow duplicates
# To create a dictionary, the curly braces({}) is used and the key : value pairs are denoted with (key : value)

person1 = {
    "name":"Brandon",
    "age": 28,
    "occupation" : "student",
    "isEnrolled": True,
    "subjects" : ["Python", "SQL", "Django"]
}

# To get the number of key-value pairs on the dictionary, the len() method
print(len(person1))

#Accesing values in dictionary
#To get item in the dictionary, the key name can be reffered using square bracket([])
print(person1["name"])
# print(person1.name)

# The keys() method will return a list of all the keys in the dictionary
print(person1.keys())

# The values method will return a list of all the values in the dictionary
print(person1.values())

# The items() method will return each item in a dictionary as key-value pair in a list 
print(person1)
print(person1.items())

# Adding key-value pairs can be done with either a new index key and assighning a value or the update method
# index key
person1["nationality"] = "Filipino"
print(person1)

#update
person1.update({"fav_food": "Sinigang"})
print(person1)

# deleting entries - it can be done using the pop() method and the del keyword

person1.pop("name")
print(person1)

#using the del keyword
del person1["nationality"]
print(person1)

# the clear method empties the dictionary
person2={
    "name" : "John",
    "age": 18
}

# person2.clear()
print(person2)

# Looping through dictionary
for prop in person2:
    print(f"The value of {prop} i {person2[prop]}")

# Nested dictionaries can be nested inside each other
person3 = {
    "name": "Monika",
    "age": 20,
    "occupation": "poet",
    "isEnrolled": True,
    "subjects": ["Python", "SQL", "Django"]
}

print(person3["subjects"][0])
print(person3["subjects"][-1])

class_room = {
    "student1": person1,
    "student2" : person3
}

print(class_room["student2"]["subjects"][1])

room1 = "Room 1"
room2 = "Room 2"

rooms = {
	room1: room1,
	room2: room2
}

print(rooms)






# [SECTION] Functions
# Functions are block of code that runs when called
# A function can be used to get inputs, process inputs and return inputs
# Imagine "def" keyword to create a function. The syntax:
# def <function_name>()

# defines a function called my_greeting

def my_greeting():
    print("hello user!")
# Calling or Invoking a function - To use a function, just specify the function name and provid the value/values needed if there are.
my_greeting()

def greet_user(username):
    print(f"hello,{username}")

#Arguments are the values that are submitted to the parameters
greet_user("Chris")

# Return statements - the "return" keyword allow functions to return values 

def addition(num1, num2):
    return num1 + num2

sum = addition(5, 9)
print(f"the sum is {sum}!")
print(addition(5, 9))

# [SECTION] Lambda Functions
# A lambda function is a small anonymous function that can be used for callbacks
# It is just like any normal python function, except that it has no name when defining it and it is contained in one of code

#A lambda function can take any number of arguments but can inly have one expression

greeting = lambda person : f'hello {person}'


print(greeting("Elsie"))

mult = lambda a, b : a * b
print(mult(5, 6))

# [SECTION] Classes
# Classes would serve as blueprints to describe the concept of objects
# Each object has characteristics (properties) and behaviors (method).

# To Create a Class, the "class" keyword is used along with the class name that starts with an uppercase character

class Car():
	# properties that all car Car objects must have are defined in a method called init

	# initializing the properties of our function
	def __init__(self, brand, model, year_of_make):	
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make

		# propeties that are hard coded
		self.fuel = "Gasoline"
		self.fuel_level = 0

	# method
	def fill_fuel(self):
		print(f'Current fuel level: {self.fuel_level}.')
		print('filing up the fuel tank . . .')
		self.fuel_level = 100
		print(f'New fuel level: {self.fuel_level}.')


	def drive(self, distance):
		print(f'The car has driven {distance} kilometers!')

		self.fuel_level = self.fuel_level - distance

		print(f'The fuel left: {self.fuel_level}')




# intantiate a class
new_car = Car("Nissan", "GT-R", 2019)

print(new_car.brand)
print(new_car.model)
print(new_car.year_of_make)

print(new_car)
# to invoke a method
new_car.fill_fuel()
print(new_car.fuel_level)

new_car.drive(50)
print(new_car.fuel_level)
